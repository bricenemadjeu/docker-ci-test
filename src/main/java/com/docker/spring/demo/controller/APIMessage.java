package com.docker.spring.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class APIMessage {

    @GetMapping("/message")
    public String getMessage(){
        return "Hello word";
    }
}
